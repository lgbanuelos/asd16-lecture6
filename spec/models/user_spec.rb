require 'rails_helper'

describe User do
  context 'Account creation' do
    it 'is valid if all user information is provided' do
      expect(build(:user)).to be_valid
    end
    it 'is invalid without a full name' do
      # expect(build(:user, :full_name => nil)).not_to be_valid
      expect(build(:user, :full_name => nil).error_on(:full_name).size).to eq(1)
    end
    it 'is invalid without a username' do
      expect(build(:user, :username => nil).error_on(:username).size).to eq(1)
    end
    it 'is invalid if duplicated username (e.g., username is already used)' do
      create(:user, :username => "gandalf")
      expect(build(:user, :username => "gandalf")).not_to be_valid
    end
    it 'is invalid if "password" and "password_confirmation" do not match' do
      user = build(:user, :password => 'password', :password_confirmation => 'x')
      expect(user.errors_on(:password_confirmation).size).to eq(1)
    end
  end
end
