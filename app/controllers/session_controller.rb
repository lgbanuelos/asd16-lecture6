class SessionController < ApplicationController
  def new
  end
  def create
    @user = User.find_by_username(params[:username])
    if @user.password == params[:password]
      redirect_to tasks_path
    else
      redirect_to login_path
    end
  end
end
